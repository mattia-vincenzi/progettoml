\section{Confronto dei Modelli}

\subsection{ROC e AUC}

Singolarmente i modelli sono stati valutati calcolando la \textbf{matrice di confusione} e le relative metriche. Manca però da stabilire quale sia il miglior modello ottenuto dopo l'addestramento. Per eseguire questo confronto tra i modelli allenati è possibile disegnare per ogni modello la \textbf{ROC Curve} (Receiver Operating Comparison Curve), che consente di esprimere quanto ogni modello discrimini correttamente le due classi. Possiamo pensare a questo grafico come la frazione di predizioni corrette per la classe positiva (asse delle y) contro la frazione di errore per la classe negativa (asse delle x). È uno strumento diagnostico usato comunemente per i classificatori su problemi di previsione binaria bilanciati e sbilanciati allo stesso modo perché non è influenzato dalla classe di maggioranza o di minoranza.

La \textbf{ROC Curve} mette a confronto il \textbf{TPR} (True Positive Rate - Recall - Sensitivity), sull'asse delle y, e il \textbf{FPR} (False Positive Rate), sull'asse delle x. Viene calcolato il rapporto tra \textbf{TPR} e \textbf{FPR} al variare di una \textbf{threshold} di classificazione. Più bassa è tale soglia, maggiore è il numero di esempi classificati come positivi. 

Queste due misure si calcolano a partire dalla matrice di confusione e rappresentano:
\begin{itemize}
    \item \textbf{TPR} (y): la percentuale di clienti classificati come insolventi rispetto al totale di quelli realmente insolventi (\textbf{recall}). 
    \item \textbf{FPR} (x): la percentuale di clienti classificati erroneamente come insolventi sul totale di quelli non insolventi. Quanti individui appartenenti alla classe "no" sono stati predetti dal classificatore come "yes".
\end{itemize}{}

Esiste un \textbf{trade-off} tra TPR e FPR, in modo tale che la modifica della soglia di classificazione comporta un cambiamento dell'equilibrio delle previsioni verso il miglioramento di TPR a spese di FPR o viceversa.

La curva ottima passa per il punto il alto a sinistra (0,1), ovvero per un TPR uguale a 1 e un FPR pari a 0. Questa situazione ottima si traduce nel classificare correttamente tutti i clienti classificati come insolventi e non commettere nessun errore nella classificazione di quelli non insolventi.

Lungo la curva la soglia diminuisce, partendo da 1.0 e arrivando a 0.0. Nel punto di inizio della curva (0,0) valendo 1.0 tutti gli esempi vengono classificati come negativi e quindi sia il TPR che il FPR sono uguali a 0. Stesso discorso, ma con risultato opposto, può essere fatto per il punto di fine della curva, nello specifico (1,1).

Sebbene la curva ROC sia uno strumento diagnostico utile, può essere difficile confrontare due o più classificatori in base alle loro curve.
Invece, di questa curva è possibile misurare l'\textbf{AUC-ROC} (Area Under ROC Curve), ovvero l'area sottesa dalla curva. Questa misura consente di fornire una misura di performance aggregata tra le varie threshold. Tanto maggiore sarà quest'area tanto il modello sarà migliore. 

Di seguito viene riportata l'immagine delle ROC Curve dei vari modelli messe a confronto sullo stesso grafico:

\begin{figure}[h!]
    \includegraphics[width=\linewidth]{img/roc_new.png}
    \centering
    \caption{\textit{Confronto ROC e AUC dei vari modelli addestrati.}}
\end{figure}

Confrontando il valore dell'\textbf{AUC} il modello migliore risulterebbe la Neural Network, a un solo punto percentuale di distanza da Naive Bayes. Questi due algoritmi sono quelli la cui curva si avvicina maggiormente all'angolo in alto a sinistra, in cui dovrebbe passare il classificatore ottimo. Tutti i modelli stanno al di sopra della retta che taglia a 45$^{\circ}$ il grafico, rappresentante il classificatore casuale (no skill classifier). 

\subsection{Curve di Precision e Recall}

Le \textbf{curve di precision e recall} (PR) sono utili per la valutazione del modello di learning quando vi sono classi sbilanciate e si ha la necessità di indagarne una in particolare. Nel dominio scelto, ad esempio, il numero di clienti insolventi è di molto inferiore al numero di clienti che ripagano i debiti.

Le curve PR sono sensibili a quale classe è definita come positiva, ottenendo un risultato molto diverso a seconda di quella impostata come tale. Nel caso illustrato la classe minoritaria "yes" (Paid) è considerata come positiva.

Al diminuire della threshold aumentano gli esempi classificati come positivi, di conseguenza aumenta la recall della classe positiva, mentre diminuisce la precision. Al contrario, all'aumentare della threshold, diminuisce la recall e la precision aumenta. Anche in questo caso sul grafico la threshold diminuisce lungo la curva.

Inoltre, per avere una misura aggregata sull'intera curva, si può calcolare l'\textbf{AUC-PR} (Area Under PR Curve). Questo score può essere utilizzato come metodo di comparazione tra modelli di classificazione binaria. Un modello con AUC-PR pari ad uno è un modello perfetto.

\begin{figure}[h!]
    \includegraphics[width=\linewidth]{img/roc-proc.png}
    \centering
    \caption{\textit{Confronto PR e AUC dei vari modelli addestrati.}}
\end{figure}

Andando a considerare l'\textbf{AUC} il miglior modello è rappresentato, anche in questo caso, dalla rete neurale poichè si vanno a considerare come migliori le curve che tendono ad avere un punto più vicino ad (1,1), ovvero il punto del classificatore ottimale.

\subsection{Confronto ROC e Confidenza}

Per i modelli di classificazione è importante avere una stima di quanto il modello sarà attendibile su dati nuovi, di conseguenza non si vuole una stima data da un singolo valore, ma bensì un \textbf{intervallo di confidenza}, ovvero un intervallo di accuratezze che avrà il modello su dati sconosciuti. Nei seguenti plot viene fissata la confidenza al 95\%; si ricerca quindi quell'intervallo tale per cui con il 95\% di probabilità/confidenza l'accuratezza reale, su dati nuovi, cadrà in esso.

Di seguito verranno riportati i dotplot raffiguranti il confronto tra i vari modelli:

\begin{figure}[h!]
    \includegraphics[width=0.9\linewidth]{img/dotplot_dt+nb.png}
    \centering
    \caption{\textit{Confronto intervalli di confidenza Decision Tree e Naive Bayes.}}
\end{figure}

In questo primo grafico vengono messi a confronto Naive Bayes e Decision Tree in termini di ROC e del suo intervallo di confidenza al 95\%, centrato sul valore dell'\textbf{AUC-ROC} ottenuto sul test set.

\begin{figure}[h!]
    \includegraphics[width=1\linewidth]{img/dotplot_svm+nn.png}
    \centering
    \caption{\textit{Confronto intervalli di confidenza SVM e Neural Network.}}
\end{figure}

Allo stesso modo in quest'ultimo grafico, vengono messi a confronto SVM e Neural Net in termini di ROC e del suo intervallo di confidenza al 95\%, centrato sul valore dell'\textbf{AUC-ROC} ottenuto sul test set. In questo caso gli intervalli sono più ampi dei precedenti, questo è dovuto al fatto che la dimensione dell'intervallo è dettata dalla dimensione dell'insieme di dati su cui il modello viene testato. Per ragioni di efficienza in termini di tempi di addestramento, come spiegato nel capitolo precedente, i due modelli in questo secondo plot (SVM e NN) sono stati testati senza effettuare la \textbf{repeatedcv} per 3 volte. In questo modo il modello è stato testato un numero di volte inferiore.

\subsection{Confronto sui tempi di addestramento}

Infine, come ultimo metodo di comparazione si mettono a confronto i tempi di addestramento. Nell'immagine riportata sono presenti due tabelle in quanto nella tabella di sinistra i modelli sono stati testati con la \textbf{repeatedcv}, mentre quelli a destra senza la ripetizione. In questi tempi si fa riferimento ad un training set costituito da circa 20'000 record. 

\begin{figure}[h!]
    \includegraphics[width=\linewidth]{img/timing.png}
    \centering
    \caption{\textit{Confronto tempi di addestramento dei quattro modelli.}}
\end{figure}

Il motivo di tale scelta è deducibile vedendo i tempi di addestramento riportati nelle tabelle. Le SVM e le Neural Net hanno impiegato tempi molto più elevati seppur essendo addestrati senza \textbf{repeatedcv}. Questa differenza è particolarmente marcata nell'addestramento con \textbf{10-fold cross validation}, ma è anche possibile notarla nell'addestramento del modello finale (in particolare nelle SVM).

\subsection{Conclusioni}

Dai grafici riportati in questo ultimo capitolo è stato possibile stabilire quale fosse il modello con le performance migliori. Sia in termini di \textbf{AUC-ROC}, che in termini di \textbf{AUC-PR} il modello migliore è stato quello basato su Reti Neurali. 

Seppur il modello Naive Bayes abbia punteggi di poco inferiori la differenze sono osservabili in termini di precision e di recall sulle matrici di confusione dei singoli modelli. Nonostante entrambi lavorino in maniera abbastanza simile sulla classe "no", il focus viene posto sulla classe minoritaria ("yes"). Mettendo a confronto l'\textbf{f-score}, come misura aggregata di precision e di recall, le Neural Network hanno un punteggio del 50\%, mentre Naive Bayes del 33\%. 

Questo dimostra come le Neural Network lavorino meglio nella classificazione di individui come insolventi, rispetto al totale di quelli realmente tali (\textbf{recall} = 43\%). Naive Bayes fornisce invece una \textbf{recall} del 22\% sulla classe "yes", ovvero di 11 punti percentuali inferiore a quella delle Reti Neurali.

