\section{Divisione dataset e analisi training set}

Una volta terminata questa analisi introduttiva del dataset, la prima cosa da fare è dividere il dataset in due parti:

\begin{itemize}
    \item \textbf{Training set}: Insieme utilizzato per indurre i nostri algoritmi di learning, dal quale cercheranno di apprendere e generalizzare.
    \item \textbf{Test set}: Insieme utilizzato per validare la conoscenza appresa durante il training.
\end{itemize}

La tecnica utilizzata per la divisione è l'\textbf{hold-out} (70\%-30\% a favore del training set).

Una volta effettuata la divisione si esegue il controllo delle righe all'interno del training e del test per verificare che la suddivisione sia avvenuta mantenendo le proporzioni.

\begin{itemize}
    \item \textbf{Numero di istanze del training set}: 21014.
    \item \textbf{Numero di istanze del test set}: 8986.
\end{itemize}

Di seguito si controlla che la distribuzione della variabile target venga mantenuta dopo la suddivisione del dataset.

\begin{figure}[h!]
    \includegraphics[width=0.8\linewidth]{img/DEFAULT_TRAINeTEST.png}
    \centering
    \caption{\textit{Distribuzione variabile target in training (sinistra) e test (destra).}}
\end{figure}

\subsection{Analisi Training Set}

\subsubsection{Plot Covariate}

È importante visualizzare i vari boxplot, mettendo a confronto le varie feature con la variabile target.

\begin{figure}[h!]
    \includegraphics[width=0.8\linewidth]{img/LIMIT_BAL-AGE+DEFAULT.png}
    \centering
    \caption{\textit{Boxplot LIMIT\_BAL e AGE.}}
\end{figure}

\begin{figure}[h!]
    \includegraphics[width=0.91\linewidth]{img/BILL_AMT+DEFAULT.png}
    \centering
    \caption{\textit{Distribuzione variabili BILL\_AMTx rispetto alla variabile target.}}
\end{figure}

\begin{figure}[h!]
    \includegraphics[width=0.91\linewidth]{img/PAY_AMT+DEFAULT.png}
    \centering
    \caption{\textit{Distribuzione variabili PAY\_AMTx rispetto alla variabile target.}}
\end{figure}

\begin{figure}[h!]
    \includegraphics[width=0.8\linewidth]{img/PAY_X+TARGET.png}
    \centering
    \caption{\textit{Distribuzione variabili PAY\_x rispetto alla variabile target.}}
\end{figure}

\newpage Come si può notare le feature prese singolarmente non permettono di fornire chiaramente una discriminazione tra le due classi, in quanto i box rappresentanti le due classi sono per lo più sovrapposti.

Di seguito vengono invece messe a confronto le feature categoriche in rapporto con la variabile target.

\begin{figure}[h!]
    \includegraphics[width=0.75\linewidth]{img/SEX+DEFAULT.png}
    \vspace{0.5cm}
    \includegraphics[width=0.75\linewidth]{img/MARRIAGE+DEFAULT.png}
    \centering
    \caption{\textit{Istogrammi che mettono a confronto ogni livello di SEX e MARRIAGE con la variabile DEFAULT.}}
\end{figure}

\begin{figure}[h!]
    \includegraphics[width=0.9\linewidth]{img/EDUCATION+DEFAULT.png}
    \centering
    \caption{\textit{Istogramma che mette a confronto ogni livello di EDUCATION  con la variabile DEFAULT.}}
\end{figure}

Anche in questo caso dall'analisi delle feature categoriche, in rapporto con la variabile target, non emerge una distribuzione all'interno dei singoli livelli differente da quella della singola variabile target. Questo significa che anche per le variabili categoriche non vi è una netta discriminazione ai fini della classificazione.

Possiamo supporre quindi che probabilmente queste variabili lavoreranno bene insieme una volta combinate nell'analisi.

Proviamo ora a capire la correlazione fra tutte le feature continue. Escludiamo da questa analisi le feature categoriche poiché, a seguito di un studio effettuato, è emerso come questa sia possibile solo per quelle variabili discrete ordinali.

\begin{figure}[h!]
    \includegraphics[width=\linewidth]{img/CORRELATION.png}
    \centering
    \caption{\textit{Correlazione calcolata su tutte le variabili numeriche del dataset.}}
\end{figure}

\newpage Osservando questo grafico è possibile vedere come le variabili appartenenti ai blocchi \textbf{PAY\_x}, \textbf{BILL\_AMTx} e \textbf{BILL\_AMTx} siano correlate fra loro. Risulta invece interessante come venga evidenziata, oltre ad altri elementi sparsi, una discreta correlazione fra il blocco delle feature \textbf{PAY\_AMTx}, quello  \textbf{BILL\_AMTx} e la variabile \textbf{LIMIT\_BAL}. Quest'ultima, al contrario, mostra un coefficiente di correlazione negativo nei confronti del blocco di feature \textbf{PAY\_x}.

\subsubsection{Plot Multivariate}

Anche in questo caso proviamo a confrontare più variabili indipendenti con la dipendente, per controllare se qualche coppia particolare di features consente di eseguire una buona discriminazione della target. Una situazione del genere si ottiene nel caso in cui i punti sono chiaramente separati nello spazio di rappresentazione.

\begin{figure}[h!]
    \includegraphics[width=1.0\linewidth]{img/MULTIVARIATES.png}
    \centering
    \caption{\textit{Analisi di più variabili numeriche messe a confronto con la target.}}
\end{figure}

\newpage Dai grafici sopra riportati si può notare che nessuna coppia di feature consente di discriminare chiaramente l'insieme di punti nelle due classi.

\subsubsection{PCA}

La \textbf{PCA} (Principal Component Analysis) è una tecnica di estrazione delle feature che consente di attuare una trasformazione lineare sul datatset, portandolo in un nuovo spazio di coordinate. Si cerca di raggiungere una riduzione della dimensionalità, trasformando un set di variabili correlate in un nuovo set di componenti principali non correlate tra loro. Ogni componente estratta sarà ortogonale alle altre. La riduzione della dimensionalità si ottiene andando ad identificare le direzioni principali, ovvero le componenti ove i dati variano maggiormente. La PCA assume infatti che la direzione che spiega maggiore varianza sia la più importante.

Essendo la \textbf{PCA} una tecnica che si applica alle feature numeriche, per prima cosa vengono selezionate solo le variabili di questo tipo prima di eseguire questa analisi.

I dati vengono quindi rappresentati in un nuovo sistema di coordinate, in cui gli assi sono rappresentati dalle componenti principali.

Una volta eseguita l'estrazione è possibile consultare gli \textbf{autovalori}; questi misurano l'ammontare di varianza spiegata da ogni componente principale precedentemente estratta.
Mediante la tabella in figura è possibile capire proporzionalmente la quantità di varianza spiegate da ogni componente. Solitamente un autovalore > 1 indica che quella componente cattura una varianza maggiore rispetto a una nei dati originali e quindi spiega bene il fenomeno. 

\begin{figure}[h!]
    \includegraphics[width=0.8\linewidth]{img/eingvalues.png}
    \centering
    \caption{\textit{Tabella autovalori.}}
\end{figure}

Per decidere quante componenti mantenere si possono utilizzare due criteri di valutazione:

\begin{itemize}
    \item Scegliere solamente le dimensioni che hanno autovalore > di 1.
    \item Fissare una percentuale di varianza spiegata e selezionare un quantitativo di componenti principali in grado di spiegarla.
\end{itemize}

Altro strumento utile a nostra disposizione è lo \textbf{screeplot} che ci consente di visualizzare componenti ordinate dalla più alla meno importante (in termini di varianza spiegata da ognuno di essi).

\begin{figure}[h!]
    \includegraphics[width=0.74\linewidth]{img/PCA.png}
    \centering
    \caption{\textit{Scree plot - istogramma che rappresenta l'ammontare di varianza spiegata per ogni componente estratta.}}
\end{figure}

E' possibile anche mostrare le variabili all'interno dello spazio bidimensionale formato dalle componenti principali. Le coordinate di queste variabili sono rappresentate dalla correlazione tra una variabile e le componenti. 

\begin{figure}[h!]
    \includegraphics[width=0.74\linewidth]{img/VARIABLES - PCA.png}
    \centering
    \caption{\textit{Proiezione delle feature nello spazio di coordinate della PCA.}}
\end{figure}

Dal grafico si può notare che i blocchi di variabili \textbf{PAY\_x}, \textbf{BILL\_ATMx} e \textbf{PAY\_ATMx} sono tutti raggruppati fra loro e quindi positivamente correlate. Inoltre un altro raggruppamento evidente è quello che comprende i blocchi di variabili \textbf{BILL\_ATMx} e \textbf{PAY\_ATMx} e, in maniera ridotta, la variabile \textbf{LIMIT\_BAL}. Tutto questo conferma ciò che emerso dalle analisi precedenti.

Infine la variabile \textbf{AGE} è la più vicina all'origine, questo implica che non è una buona variabile, in quanto cattura poca varianza dei nostri dati.

\bigskip
E' inoltre possibile rappresentare in questo nuovo spazio anche i singoli "individui", ovvero le singole osservazioni. Cercando di raggrupparli in base al valore della variabile target.

\begin{figure}[h!]
    \includegraphics[width=1.0\linewidth]{img/individuals.png}
    \centering
    \caption{\textit{Proiezione dellle singole osservazioni del training set nello spazio delle PCA.}}
\end{figure}

\newpage

\subsubsection{Definizione delle variabili incluse nell'analisi}

Dalle analisi effettuate abbiamo individuato quattro possibili soluzioni per la scelta delle variabili da utilizzare in fase di addestramento:

\begin{enumerate}
    \item Proseguire con l'analisi non eliminando nessuna variabile e quindi continuare mantenendole tutte.
    \item Addestrare il modello sul nuovo spazio prodotto dalle PCA, evitando di considerare le variabili categoriche nell'induzione di modelli.
    \item In base all'analisi della correlazione fatta precedentemente e alla PCA possiamo vedere le variabili fortemente correlate tra loro e proseguire nell'analisi rimuovendone alcune tra quelle la cui correlazione è massima (ad esempio mantenere una variabile \textbf{BILL\_AMTx} e rimuovere anche qualche PAY\_x), evitando di effettuare selezioni per quanto riguarda invece le feature categoriche.
    \item Rimuovere le variabili molto correlate tra loro ed eseguire un'analisi intuitiva anche su quelle categoriche. Si potrebbe ad esempio considerare di rimuovere \textbf{SEX} e \textbf{MARRIAGE} in quanto in base al dominio del problema non sembrano rilevanti.
\end{enumerate}

La terza opzione è quella che abbiamo considerato come più plausibile nella nostra situazione. Di conseguenza dalla matrice di correlazione tra tutte le variabili numeriche è possibile notare una zona di correlazione molto alta (colore rosso acceso) tra le variabili \textbf{BILL\_AMTx} che esprimono la somma da pagare al x-esimo mese precedente. Questa situazione di correlazione era osservabile anche nella PCA, in particolare nel grafico rappresentante le variabili nello spazio delle componenti principali. Il gruppo dei vettori \textbf{BILL\_AMTx} è infatti quasi totalmente sovrapposto. In questo senso si è deciso di mantenere solamente una di queste variabili nello specifico \textbf{BILL\_AMT4}).

Abbiamo ritenuto di poter fare questa selezione perchè un'alta correlazione tra le variabili indica la presenza di ridondanza nei dati e l'apporto di poco contenuto informativo aggiuntivo. 

Per la stessa ragione abbiamo deciso anche di rimuovere la variabile \textbf{PAY\_5} in quanto altamente correlata sia con \textbf{PAY\_4} che \textbf{PAY\_6}.
