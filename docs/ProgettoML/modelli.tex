\section{Addestramento modelli}

L'addestramento sul training set precedentemente definito è stato eseguito con 4 modelli di learning supervisionato, che di seguito verranno presentati nel dettaglio:

\begin{enumerate}
    \item \textbf{SVM}.
    \item \textbf{Naive Bayes}.
    \item \textbf{Decision Tree}.
    \item \textbf{Neural Network}.
\end{enumerate}{}

I modelli sono stati tutti addestrati utilizzando la \textbf{K Fold Cross Validation}, ovvero una tecnica che prevedere l'addestramento di K modelli di learning e di ottenere K misure di accuratezza. In questo modo si ottiene una stima più robusta dell'accuratezza del modello. La tecnica utilizzata prevede un valore di K = 10 e implica 10 split dell'insieme di training (fold) e ad ogni iterazione uno di questi viene utilizzato come test set, mentre il modello addestrato sugli altri K-1 (9) fold. Questo procedimento viene iterato per 10 volte, al fine di avere una misura di errore che è la media delle 10 ottenute. Questa stima sarà più robusta rispetto alle singole ottenute. 
Nell'implementazione utilizzata la tecnica viene applicata 3 volte. Questo è stato testato in tutti e 4 i modelli scelti, ma per esigenze nei tempi di addestramento l'esecuzione della k-fold cross validation per 3 volte è stata mantenuta solo in Naive Bayes e negli alberi decisionali. Dai modelli di SVM e Neural Network è stata rimossa la ripetizione per esigenze in termini di tempi di addestramento (troppo lunghi per essi).

Qui sotto viene riportato il codice utilizzato per impostare questa tecnica utilizzata in training con i relativi parametri. La funzione utilizzata è \textbf{\href{https://www.rdocumentation.org/packages/caret/versions/6.0-85/topics/trainControl}{traincontrol}} del package Caret.

I parametri da noi utilizzati sono:
\begin{itemize}
    \item \textbf{method}: Metodo di campionamento. Fissato "repeatedcv" per eseguire la cross validation ripetuta, "cv" invece per eseguirne una sola iterazione.
    \item \textbf{number}: 10 - Numero di folds.
    \item \textbf{repeats}: 3 - Numero di volte che viene ripetuta la cross validation (Parametro omesso per SVM e Neural Network).
    \item \textbf{classProbs}: TRUE - Definisce se le probabilità di classe devono essere calcolate per il modello in ogni ricampionamento (serve per calcolare la ROC nel train).
    \item \textbf{summaryFunction}: Funzione per ricalcolare le metriche di performance tra i ricampionamenti. Viene settato a \textit{twoClassSummary}, ovvero calcola sensitività, specificità e area sotto la curva ROC (Richiede classProbs = TRUE).
    \item \textbf{savePrediction}: "final" - Utilizzato per mantenere salvate le predizioni di probabilità inerenti al miglior modello realizzato (serve per calcolare il grafico per il confronto delle ROC).
\end{itemize}{}

\begin{lstlisting}
dt.control = trainControl(method = "repeatedcv", number = 10,repeats = 3, classProbs = TRUE, summaryFunction = twoClassSummary, savePredictions = "final")
\end{lstlisting}

\bigskip
Per addestrare i vari modelli è stata utilizzata la funzione \textbf{\href{https://www.rdocumentation.org/packages/caret/versions/6.0-85/topics/train}{train}} del package Caret.
Prima di dare in pasto i dati di training al modello conviene standardizzarli, in quanto quella che viene calcolata nello spazio è una sorta di similarità. Se sono presenti feature con unità di misure diverse allora vengono calcolati dei valori di similarità sbagliati.

Nello specifico questi sono i campi utilizzati per la funzione d'addestramento dei vari modelli:

\begin{itemize}
    \item \textbf{method}: modello da utilizzare per la classificazione, quelli da noi utilizzati sono:
        \begin{itemize}
            \item rpart: alberi decisionali.
            \item svmLinear2: Support Vector Machines with Linear Kernel.
            \item naive\_bayes: Naive Bayes.
            \item mlpML: Multi-Layer Perceptron.
        \end{itemize}{}
    \item \textbf{metric}: "ROC" - stringa che specifica la metrica che sarà usata per scegliere il modello ottimale.
    \item \textbf{preProcess}: ("scale", "center") - funzioni di preprocessing da applicare. In questo caso questi parametri consentono di realizzare la formula di scalatura e centering sui dati iniziali.
    \item \textbf{trControl}: lista di valori che indica come la funzione deve agire.
    \item \textbf{tuneGrid}: valori di tuning.
\end{itemize}{}

Di seguito, per quanto riguarda la valutazione dei singoli modelli viene riportata per ognuno di essi la \textbf{matrice di confusione} con le relative metriche associate: \textbf{accuratezza}, \textbf{precision}, \textbf{recall} e \textbf{F-score}. In questo modo è possibile valutare le performance dei modelli sulle singole classi, evitando di osservare solamente l'accuratezza che, come misura globale, non fornisce un buon metodo di analisi per problemi con classi sbilanciate. 

\begin{itemize}
    \item \textbf{Precision}: la precision indica la proporzione di individui classificati correttamente come positivi sul totale di quelli classificati come positivi.
    \item \textbf{Recall}: la recall indica la proporzione di individui classificati come positivi sul totale di quelli realmente positivi.
    \item \textbf{F-score}: misura globale per bontà dei modelli di classificazione. Solitamente è attribuito il medesimo peso ad entrambe le misure ma è possibile cambiare i coefficienti nel caso si voglia dare più importanza ad una. Media armonica di Precision e Recall. 
\end{itemize}{}

Nella descrizione di queste misure si fa riferimento solo alla classe positiva, ma è utile calcolarle su entrambe le classi. Questo ci consente di capire le misure di performance per ogni singola classe. Si ha quindi un’informazione di dettaglio molto più grande. È possibile avere delle misure sulle singole classi, cosa che con le misure globali non è possibile.

\subsection{SVM}

Le \textbf{SVM} (Support Vector Machines) rappresentano una classe di modelli di learning supervisionato che costruiscono l'iperpiano di separazione "migliore" tra due classi di punti, individuando i \textit{support vector} che determinano il massimo margine. Questo modello di learning va alla ricerca della soluzione ottima, ovvero dell'iperpiano meno soggetto ad \textbf{overfitting}, quello di massimo bordo. 

Nella formulazione più semplice delle SVM non sono ammessi errori, ovvero istanze missclassified, mentre nella formulazione più comune, invece, alcuni errori sono ammessi e proprio per questo si effettua il tuning dell'iperparametro C (costo), che determina il trade-off tra le variabili di slack che caratterizzano l'errore e la dimensione del margine. C mi consente di avere un margine più o meno ampio a seconda del numero di errori ammessi:

\begin{itemize}
    \item Un valore piccolo di C crea un margine ampio (\textbf{soft margin}), ovvero permette più errori di missclassification.
    \item Un valore grande di C allora si crea un margine stretto (\textbf{hard margin}) e sono tollerati pochi errori.
\end{itemize}{}

Di seguito viene riportato il codice utilizzato per l'addestramento delle SVM. I valori di costo testati sono quelli indicati con la funzione \textbf{expand.grid()}.

\begin{lstlisting}
svm.grid = expand.grid(cost = c(0.5, 1, 10))
svm.model= train(DEFAULT ~ ., data = trainset, preProcess=c("scale", "center"), method = "svmLinear2",  metric = "ROC", trControl = svm.control, tuneGrid=svm.grid)
\end{lstlisting}

Al termine dell'addestramento viene riportato il seguente grafico:

\begin{figure}[h!]
    \includegraphics[width=0.7\linewidth]{img/tuning_svm.png}
    \centering
    \caption{\textit{Parametro C a confronto con AUC-ROC.}}
\end{figure}

Questo grafico mostra come al variare di C cambia l'area sottesa dalla AUC-ROC. Maggiore è questo valore, migliore sarà il modello.
Il miglior modello si è ottenuto con un valore di \textbf{C = 0.5}.
Inizialmente era nostra intenzione utilizzare più valori per il tuning del costo ma, a causa dei tempi richiesti per l'addestramento, il loro numero è stato ridotto.

\bigskip
Di seguito viene riportata la matrice di confusione con le relative metriche:

\begin{figure}[h!]
    \includegraphics[width=\linewidth]{img/svm-evaluation.png}
    \centering
    \caption{\textit{Matrice di confusione e relative metriche SVM.}}
\end{figure}

Per quanto riguarda la misura globale, raggiunge un livello di accuratezza dell'81\%, ma nel caso di problemi sbilanciati è importante valutare la precision e la recall su entrambi i valori della variabile target:

\begin{itemize}
    \item Classe \textbf{yes}: sulla classe minoritaria il classificatore SVM commette molti errori in quanto solo il 34\% degli individui realmente della classe "yes" vengono predetti come tali (\textbf{recall}). Molti di questi vengono erroneamente predetti come negativi. Sul totale degli individui classificati come "yes" il classificatore ne classifica correttamente il 64\% (\textbf{precision}).
    \item Classe \textbf{no}: sulla classe maggioritaria il classificatore SVM classifica correttamente come "no" il 93\% degli individui realmente della classe "no" (\textbf{recall}). Sul totale di quelli classificati come "no" dal SVM l'84\% viene classificato correttamente come tale (\textbf{precision}). 
\end{itemize}{}

Di seguito viene mostrata anche la ROC con la threshold migliore, che porta al miglior rapporto tra \textit{True Positive Rate} e \textit{False Positive Rate}. 

\begin{figure}[h!]
    \includegraphics[width=\linewidth]{img/topleft_svm.png}
    \centering
    \caption{\textit{Threshold migliore mostrata sulla curva ROC.}}
\end{figure}

\subsection{Decision Tree}

Questo algoritmo di learning supervisionato ha come obiettivo la creazione di un albero decisionale consistente con gli esempi di training. Nella libreria \textbf{Rpart} ricorsivamente viene scelto l'attributo più significativo utilizzando \textbf{l'indice di Gini}. Gli alberi di decisione sono spesso utilizzati in quanto facilmente interpretabili. 

Di seguito viene riportato il codice per l'addestramento dell'albero di decisione. 

\begin{lstlisting}
dt.model= train(DEFAULT ~ ., data = trainset, preProcess=c("scale", "center"), method = "rpart", metric = "ROC", trControl = dt.control)
\end{lstlisting}

Molto importante in questi alberi è il parametro di complessità \textbf{cp}, questo parametro funge da penalità per controllare la dimensione dell'albero. Maggiore è il valore di cp, minore è il numero di split presenti.
Può essere considerato un tasso d'errore e per questa ragione una buona tecnica per ottimizzare questo tipo di modello è quello di fare il \textbf{pruning}, ovvero tagliare l'albero, alla profondità con cp minore.

Questo parametro viene ottimizzato in automatico, in quanto vengono calcolati i vari cp e il l'albero viene tagliato sulla base di quello migliore. Il metodo \textbf{train} di Caret esegue già in automatico il tuning di alcuni iperparametri di default per ogni algoritmo di learning e nel caso dell'albero decisionale effettua proprio il pruning sulla base del cp migliore.

Il grafico che mostra la qualità del modello al variare del valore del cp è il seguente:

\begin{figure}[h!]
    \includegraphics[width=\linewidth]{img/tuning_dt.png}
    \centering
    \caption{\textit{Parametro cp a confronto con AUC-ROC.}}
\end{figure}

Il miglior modello si è ottenuto con un valore di \textbf{cp = 0.003024411}.

L'albero ottenuto dal training viene mostrato nella figura seguente:

\begin{figure}[h!]
    \includegraphics[width=0.7\linewidth]{img/dt.png}
    \centering
    \caption{\textit{Plot dell'albero dopo il pruning ottenuto dall'addestramento.}}
\end{figure}

La matrice di confusione con le relative misure è la seguente:

\begin{figure}[h!]
    \includegraphics[width=0.9\linewidth]{img/dt-evaluation.png}
    \centering
    \caption{\textit{Matrice di confusione e relative metriche.}}
\end{figure}

Per quanto riguarda la misura globale, raggiunge un livello di accuratezza dell'81\%, come nel caso precedente è importante analizzare le classi prese singolarmente:

\begin{itemize}
    \item Classe \textbf{yes}: sulla classe minoritaria l'albero decisionale commette molti errori in quanto solo il 38\% degli individui realmente della classe "yes" vengono predetti come tali (\textbf{recall}). Molti di questi vengono erroneamente predetti come negativi. Sul totale degli individui classificati come "yes" il classificatore ne classifica correttamente il 65\% (\textbf{precision}).
    \item Classe \textbf{no}: sulla classe maggioritaria l'albero decisionale classifica correttamente come "no" il 94\% degli individui realmente della classe "no" (\textbf{recall}). Sul totale di quelli classificati come "no" dall'albero decisionale l'84\% viene classificato correttamente come tale (\textbf{precision}). 
\end{itemize}{}

Di seguito viene mostrata anche la ROC con la threshold migliore, che porta al miglior rapporto tra \textit{True Positive Rate} e \textit{False Positive Rate}. 

\begin{figure}[h!]
    \includegraphics[width=\linewidth]{img/topleft_dt.png}
    \centering
    \caption{\textit{Threshold migliore mostrata sulla curva ROC.}}
\end{figure}

\subsection{Naive Bayes}

E' un algoritmo supervisionato di classificazione che stima la probabilità di eventi futuri utilizzando conoscenze a priori. Questo metodo è utilizzato per un compito di learning in cui ogni istanza è descritta da una congiunzione di valori degli attributi e dove la funzione target può assumere qualsiasi valore di un insieme finito. Questo algoritmo utilizza della conoscenza a priori che può essere stimata sui dati di training, inoltre assume che gli attributi siano condizionalmente indipendenti (ipotesi forte). 

L'algoritmo andrà quindi a stimare le probabilità a priori (\textbf{prior}) sul training set e, successivamente, per ogni attributo e per ogni valore da esso assunto, calcola la \textbf{likelihood}. Questo insieme di probabilità calcolate verranno poi utilizzate nel momento in cui saranno da classificare nuove istanze.

Di seguito viene mostrato il codice utilizzato per l'addestramento dell'algoritmo. Il tuning viene effettuato sui seguenti iperparametri:
\begin{itemize}
    \item \textbf{useKernel}: valori ammissibili solamente TRUE e FALSE, indicano la il tipo di distribuzione.
    \item \textbf{laplace}: lista di possibili valori da utilizzare come correlazione di laplace.
    \item \textbf{adjust}: lista di possibili valori da utilizzare come regolazione della larghezza di banda.
\end{itemize}{}

Per queste liste di parametri i valori sono stati scelti approfondendo la ricerca su varie fonti online.

\begin{lstlisting}
nb.grid = expand.grid(usekernel=c(TRUE, FALSE), laplace=0:5, adjust = seq(0, 5, by = 1))
nb.model= train(DEFAULT ~ ., data = trainset, preProcess=c("scale", "center"), method = "naive_bayes", metric = "ROC", trControl = nb.control, tuneGrid=nb.grid)
\end{lstlisting}

\bigskip
Il miglior modello è stato ottenuto con un valore di \textbf{useKernel} = TRUE, \textbf{laplace} = 0 (indipendente, stesso risultato con tutti) e di \textbf{adjust} = 5.

\newpage
L'immagine seguente lo mostra graficamente:

\begin{figure}[h!]
    \includegraphics[width=0.8\linewidth]{img/tuning_nb.png}
    \centering
    \caption{\textit{Tuning iperparametri Naive Bayes a confronto con AUC-ROC.}}
\end{figure}

La matrice di confusione con le relative metriche è la seguente:

\begin{figure}[h!]
    \includegraphics[width=0.85\linewidth]{img/nb-evaluation.png}
    \centering
    \caption{\textit{Matrice di confusione e relative metriche.}}
\end{figure}

Per quanto riguarda la misura globale, raggiunge un livello di accuratezza del 79\%, come nei casi precedenti è importante analizzare le classi prese singolarmente:

\begin{itemize}
    \item Classe \textbf{yes}: sulla classe minoritaria Naive Bayes commette moltissimi errori in quanto solo il 22\% degli individui realmente della classe "yes" vengono predetti come tali (\textbf{recall}). Molti di questi vengono erroneamente predetti come negativi. Sul totale degli individui classificati come "yes" il classificatore ne classifica correttamente il 64\% (\textbf{precision}).
    \item Classe \textbf{no}: sulla classe maggioritaria Naive Bayes classifica correttamente come "no" il 96\% degli individui realmente della classe "no" (\textbf{recall}). Sul totale di quelli classificati come "no" da Naive Bayes l'81\% viene classificato correttamente come tale (\textbf{precision}). 
\end{itemize}{}

Di seguito viene mostrata anche la ROC con la threshold migliore, che porta al miglior rapporto tra \textit{True Positive Rate} e \textit{False Positive Rate}. 

\begin{figure}[h!]
    \includegraphics[width=\linewidth]{img/topleft_nb.png}
    \centering
    \caption{\textit{Threshold migliore mostrata sulla curva ROC.}}
\end{figure}

\subsection{Neural Network}

Prima di poter addestrare questo modello di learning supervisionato è necessario applicare ai dati una tecnica chiamata \textbf{One Hot Encoding} che consiste nel trasformare ogni feature categorica in un insieme di attributi numerici, uno per ogni livello assunto dalla feature in esame. Questo passaggio è necessario per poter addestrare una Neural Net in quanto il layer di input deve essere costituito solamente da feature numeriche. 

Questa tecnica nella libreria utilizzata viene applicata di default ai dati in ingresso.

Le reti neurali sono costituite da gruppi di nodi interconnessi. Il primo layer è quello di input, mentre quello finale è quello di output. Tra questi vi possono essere diversi livelli intermedi, ognuno costituito da un numero arbitrario di neuroni. Ogni neurone computa una funzione di attivazione non lineare e l'informazione si propaga dallo strato di input verso quello di output.

Di seguito viene riportato il codice utilizzato per l'addestramento. Gli iperparametri che si cercano di ottimizzare con questo procedimento sono: \textbf{Layer1}, \textbf{Layer2} e \textbf{Layer3}, ovvero il numero di neuroni per ogni strato nascosto. 

\begin{lstlisting}
nn.grid = expand.grid(layer1 = 1:8, layer2 = 1:8, layer3 = 1:8)
nn.model= train(DEFAULT ~ ., data = trainset, preProcess=c("scale", "center"), method = "mlpML", metric = "ROC", trControl = nn.control, tuneGrid=nn.grid)
\end{lstlisting}

\bigskip
Il miglior modello è stato ottenuto con i seguenti valori: \textbf{Layer1 = 5},  \textbf{Layer2 = 6} e \textbf{Layer3 = 8}.

\newpage Il plot ottenuto al termine dell'addestramento mostra graficamente questo risultato ed è il seguente:

\begin{figure}[h!]
    \includegraphics[width=0.75\linewidth]{img/tuning_nn.png}
    \centering
    \caption{\textit{Tuning iperparametri Neural Net a confronto con AUC-ROC.}}
\end{figure} 

Il plot della rete ottenuto dall'addestramento è il seguente:

\begin{figure}[h!]
    \includegraphics[width=0.87\linewidth]{img/neural network.png}
    \centering
    \caption{\textit{Plot della rete ottenuta dall'addestramento.}}
\end{figure}

Come è possibile vedere le feature in ingresso sono state trasformate, ad ogni categorica sono state associate più numeriche, in base ai livelli da essa assunti (\textbf{One Hot Encoding}).
Negli strati nascosti il numero di neuroni è esattamente quello ottenuto dal tuning (in ordine nei layer rispettivamente 5, 6, 8).

\bigskip
La matrice di confusione è la seguente:

\begin{figure}[h!]
    \includegraphics[width=\linewidth]{img/nn-evaluation.png}
    \centering
    \caption{\textit{Matrice di confusione e relative metriche.}}
\end{figure}

Per quanto riguarda la misura globale, raggiunge un livello di accuratezza dell'81\%, come nei casi precedenti è importante analizzare le classi prese singolarmente:

\begin{itemize}
    \item Classe \textbf{yes}: sulla classe minoritaria la rete neurale commette abbastanza errori in quanto solo il 43\% degli individui realmente della classe "yes" vengono predetti come tali (\textbf{recall}). Molti di questi vengono erroneamente predetti come negativi. Sul totale degli individui classificati come "yes" il classificatore ne classifica correttamente il 60\% (\textbf{precision}).
    \item Classe \textbf{no}: sulla classe maggioritaria la rete neurale classifica correttamente come "no" il 91\% degli individui realmente della classe "no" (\textbf{recall}). Sul totale di quelli classificati come "no" dalla la rete neurale l'85\% viene classificato correttamente come tale (\textbf{precision}). 
\end{itemize}{}

Di seguito viene mostrata anche la ROC con la threshold migliore, che porta al miglior rapporto tra \textit{True Positive Rate} e \textit{False Positive Rate}. 

\begin{figure}[h!]
    \includegraphics[width=\linewidth]{img/topleft_nn.png}
    \centering
    \caption{\textit{Threshold migliore mostrata sulla curva ROC.}}
\end{figure}
